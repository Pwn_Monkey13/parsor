<?php

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 16/04/2016
 * Time: 22:46
 */
class Project_Model extends CI_Model
{
    public function __construct(){
        parent::__construct();
    }

    public function get_projects(){
        $this->db->from('projects');
        $this->db->select('*');
        return $this->db->get()->result();

    }

    public function get_project($idp){
        $this->db->from('projects');
        $this->db->select('*');
        $this->db->where('id', $idp);
        return $this->db->get()->row();

    }

    public function get_project_by_id($idp){
        $this->db->from('projects');
        $this->db->select('*');
        $this->db->where('id', $idp);
        return $this->db->get()->row();

    }

    public function check_is_free_project($name){
        $this->db->from('projects');
        $this->db->where('name',$name);
        $res = $this->db->get()->result();
        if(count($res) == 0 && $res == null){
            return true;
        }
        else return false;
    }

    public function rm_project($idp){
        $this->db->delete('projects ', array('id' => $idp));
    }

    public function add_project($name){
        $this->db->insert('projects',array('name' => $name));
    }

    public function get_configs($idp){
        $this->db->from('config');
        $this->db->where('id_project',$idp);
        return $this->db->get()->result();
    }

}