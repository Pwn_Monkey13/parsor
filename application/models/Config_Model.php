<?php

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 16/04/2016
 * Time: 22:41
 */
class Config_Model extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function get_config($name){
        $this->db->from('config');
        $this->db->where('name',$name);
        return $this->db->get()->row();
    }


    public function get_config_by_id($idc){
        $this->db->from('config');
        $this->db->where('id',$idc);
        return $this->db->get()->row();
    }


    public function check_is_free_configname($idp, $name){
        $this->db->from('config');
        $this->db->where('name',$name);
        $this->db->where('id_project',$idp);
        $res = $this->db->get()->result();
        if(count($res) == 0 && $res == null){
            return true;
        }
        else return false;
    }

    public function add_config($idp, $name, $link, $elements, $type){
        $this->db->insert('config',array('id_project' => $idp, 'name' => $name, 'link' => $link,
                                                'elements' => $elements, 'type' => $type));
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function alter_config($idc, $data){
        $this->db->where('id', $idc);
        $this->db->update('config', $data);
    }
    public function rm_config($idconfig){
        $this->db->delete('config',array('id' => $idconfig));
    }

    public function save_changes($idc, $json)
    {
        $data = array('flux_match' => $json);
        $this->db->where('id', $idc);
        $this->db->update('config', $data);
    }

}