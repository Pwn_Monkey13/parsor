<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 12/04/2016
 * Time: 15:22
*/

$n = (isset($_POST['nameConf'])) ? $this->input->post('nameConf') : null;
$l = (isset($_POST['link'])) ? $this->input->post('link') : null;
$el = (isset($_POST['elements'])) ? $this->input->post('elements') : $comma_str;
?>

<div id="intro">
    <?= form_open('Parsor/CreateConfig/'.$cp->id); ?>
    <div class="form-group">
        <p class="error"> <?= (isset($error) ? 'Erreur : '.$error : null); ?> </p>
        <p class="error"> <?= validation_errors(); ?> </p>
    </div>

    <div class="form-group">
        <label class="control-label">Projet :</label>
        <p class="form-control-static"><?= ucfirst($cp->name) ?> </p>
        <input type="hidden" id="hidden_idp" value="<?= $cp->id?>">
    </div>

    <div class="form-group">
        <label for="nameConf">Nom de la configuration :</label>
        <input type="text" class="form-control" id="nameConf" name="nameConf" placeholder="exemple:Config1" value="<?= $n ?>">
    </div>

    <div class="form-group">
        <label for="link">Lien du Flux à Parser:</label>
        <input type="text" class="form-control" id="link" name="link" value="<?= $l ?>" placeholder="exmple : http://wcf.tourinsoft.com/Syndication/cdt64/e2de5f38-4d06-4e64-af61-5bd9dee30978/Objects">
    </div>

    <div class="form-group">
        <label for="link">Elements du Flux:</label>
        <input type="text" class="form-control" id="elements" name="elements" value="<?=$el?>">
    </div>



    <?= anchor('Parsor', 'Retour', array('class' => 'btn btn-primary'));?>
    <button class="btn btn-primary" id="CreateConf" >Créer</button>
    <?= form_close(); ?>
</div>

