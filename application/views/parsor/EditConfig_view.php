<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$st = (isset($state) ? $state : '');
$ph = "ex foo,bar,...   " ;
$el = (isset($_POST['elements'])) ? $_POST['elements'] : $cc->elements;
$rec =(isset($_POST['rec'])) ? $_POST['rec'] : $cc->base_path;
$pathbase =(isset($_POST['pathbase'])) ? $_POST['pathbase'] : $cc->base_path;
$xmlns =(isset($_POST['xmlns'])) ? $_POST['xmlns'] : $cc->namespace;
?>
    <div id="msg_js">
        <p id="js_error" class="error"> </p>
    </div>

<?= form_open('Parsor/EditConfig/'.$cc->id, array('id' => 'EditorForm'));?>

    <div class="form-group">
        <label class="control-label">Projet :</label>
        <p class="form-control-static"><?= ucfirst($cp->name)?> </p>
        <input id="hiddenIdp" type="hidden" value="<?= $cp->id?>">
    </div>

    <div class="form-group">
        <label for="nameConf">Nom de la configuration :</label>
        <input type="text" class="form-control" id="nameConfEdit" value="<?= $cc->name?>">
        <input id="hiddenIdConf" type="hidden" value="<?= $cc->id?>">
    </div>

    <div class="form-group">
        <label for="link">Lien du Flux à Parser:</label>
        <p class="form-control-static static"> <?= $cc->link?> </p>
    </div>

    <div class="form-group">
        <label for="link">Type de données:</label>
        <p class="form-control-static"> <?= $cc->type?> </p>
    </div>

    <div class="form-group">
        <label for="link">Elements du Flux:</label>
        <input type="text" class="form-control" id="linkEdit" name="elements" value="<?= $el ?>">
    </div>

<?php if($cc->type == 'json'):?>
    <div class="form-group">
        <label for="rec">Element récurrent : <p class="required"> (*requis)</p> </label>
        <input type="text" class="form-control" id="rec" name="rec" placeholder="ex : entry, ObjectsTouristiques" value="<?= $rec ?>">
    </div>
<?php elseif($cc->type == 'xml'):?>
    <div class="form-group">
        <label for="xmlns">Namespace global : <p class="required"> (*requis)</p> </label>
        <input type="text" class="form-control" id="xmlns" name="xmlns" placeholder="ex : feed" value="<?= $xmlns ?>">
    </div>

    <div class="form-group">
        <label for="pathbase">Path de l'élement récurrent : <p class="required"> (*requis)</p> </label>
        <input type="text" class="form-control" id="pathbase" name="pathbase" placeholder="ex : //feed:entry/feed:content/m:properties" value="<?= $pathbase ?>">
    </div>

<?php endif; ?>
    <div class="form-group">
        <label for="link">Json:</label>
        <p class="form-control-static static"> <?=htmlentities($cc->flux_match)?> </p>
    </div>



<div class="form-group">
    <label for="link">Activer le controle des données:</label>
    <?php
        $data = array(
            'name'        => 'control',
            'type'        => 'radio',
            'value'       => 'oui',
            'checked'     => TRUE
        );
        echo 'Oui '. form_checkbox($data);

    $data = array(
        'name'        => 'control',
        'type'        => 'radio',
        'value'       => 'non'
    );

    echo 'Non : '. form_checkbox($data);
    ?>
</div>

    <div class="col-lg-12">
        <div id="construct">
            <h3> Affiliation des attributs:</h3>

            <?= validation_errors(); ?>
            <?php if(isset($error)){
                foreach($error as $er){
                    echo '<p class="error">'.$er.'</p>';
                }
            }
            else echo '';

            if(isset($success)){
                foreach($success as $su){
                    echo '<p class="success">'.$su.'</p>';
                }
            }
            else echo ''; ?>
            <input type="hidden" id="hiddenState" value="<?= $st ?>">

            <table id="bng" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Element BNG</th>
                    <th>Array Path</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $a = json_decode($cc->flux_match);
                foreach($elements as $item) {
                   $re = isset($_POST[$item]) ? $this->input->post($item) : (isset($a->$item) ? $a->$item : '');

                    echo '<tr> <th class="struct">'.$item.'</th> <th id="'.$item.'" class="flux">',
                    '<div class="form-group">',
                        form_input($item, $re,
                                    array("class=" => "form-control","size" => "80")),
                    '</div> </th></tr>';
                }
                ?>
                </tbody>
            </table>

            <?= anchor('Showroom', 'Retour à l\'accueil', array('class' => 'btn btn-primary')); ?>
            <?= form_submit('mysubmit', 'Enregistrer les modifications', array('class' => 'btn btn-success'));?>
        <?= form_close(); ?>

            <?= anchor('Showroom/Preview/'.$cc->id, 'Prévisualtion', array('class' => 'btn btn-info')); ?>
        </div>
    </div>