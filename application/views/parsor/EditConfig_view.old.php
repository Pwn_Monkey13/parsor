<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>

<div id="intro">
    <div class="form-group">
        <label class="control-label">Projet :</label>
        <p class="form-control-static"><?= ucfirst($cp->name)?> </p>
    </div>

    <div class="form-group">
        <label for="nameConf">Nom de la configuration :</label>
        <input id="hiddenIdConf" type="hidden" value="<?= $cc->id?>">
        <p class="form-control-static"> <?= $cc->name ?> </p>
    </div>

    <div class="form-group">
        <label for="link">Lien du Flux à Parser:</label>
        <p class="form-control-static"> <?= $cc->link?> </p>

    </div>

    <div class="form-group">
        <label for="link">Elements du Flux:</label>
        <p class="form-control-static"> <?= json_encode($this->def_str) ?> </p>
    </div>

    <div class="form-group">
        <label for="link">Json:</label>
        <p class="form-control-static"> <?=htmlentities($cc->flux_match)?> </p>
    </div>

</div>
<div id="construct">
    <h3> Affiliation des attributs:</h3>

    <table id="bng" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Element BNG</th>
            <th>Elements récupérés depuis le flux</th>
        </tr>
        </thead>
        <tbody>
            <?php
            $attr = 'id="selectDATA"';

            foreach($this->def_str as $item) {
                echo '<tr> <th class="struct">' . $item . '</th>';

                if($cc->flux_match != null){
                    $o = json_decode($cc->flux_match);
                    $a = (array) $o->{$item};
                    $title = current(array_flip($a));
                    $opt = $a[$title];
                }
                else{
                    $opt = '';
                }
                echo '<th class="flux">' . form_dropdown('ParsedFlux', $parsed,$opt, $attr) . ' </th>';
            }
            ?>
        </tbody>
    </table>

    <div class="bottom">
        <button id="save" class="btn btn-control"> Enregistrer les modifications</button>
        <input id="save_hidden" type="hidden" value="<?= site_url('Ajax/save_changes');?>">
        <?= anchor('Showroom/Preview/'.$cc->id, 'Prévisualisation',
                                    array('id' => 'preview', 'class' => 'btn btn-primary')); ?>
    </div>
</div>
