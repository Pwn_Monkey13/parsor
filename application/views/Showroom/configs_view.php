<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 13/04/2016
 * Time: 13:54
 */
?>
<h3> Selectionnez une configuration de flux</h3>

<div class="col-lg-6 col-lg-offset-3">
    <div class="row">
        <ul id="configs" class="list">
            <?php foreach($configs as $config){
                echo '<li id="'.$config->id.'" class="config">',
                        '<a class="rm_config_cross" href="" >',
                            '<svg> <circle cx="12" cy="12" r="11" stroke="black" stroke-width="2" fill="white" />',
                                '<path stroke="black" stroke-width="4" fill="none" d="M6.25,6.25,17.75,17.75" />',
                                '<path stroke="black" stroke-width="4" fill="none" d="M6.25,17.75,17.75,6.25" />',
                            '</svg>',
                        '</a>',

                    '<a href="/index.php/Parsor/EditConfig/'.$config->id.'" class="btn_config">',
                    '<span>'.ucwords($config->name).'<i class="fa fa-chevron-right"></i></span>',
                    '<span>'.$config->link.'</span></a> </li>';
            }
            ?>
        </ul>

        <ul id="projects_edit">
            <li class="project_edit" id="project_add">
                <?= anchor('Parsor/CreateConfig/'.$cp->id, 'Nouvelle configuration', array('id' =>'createConfig', 'class' => 'btn btn-primary')); ?>
            </li>
            <li class="project_edit" id="project_add">
                <button class="btn btn-primary" id="rm_config"> Supprimer une configuration </button>
            </li>
        </ul>

        <div class="col-lg-12">
            <?= anchor('Showroom', 'Retour', array('class' => 'btn btn-primary')); ?>
        </div>
    </div>
</div>




