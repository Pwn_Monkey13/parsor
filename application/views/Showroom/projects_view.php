<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 13/04/2016
 * Time: 13:54
 */
?>
    <h3>Projets BNG</h3>

    <div class="col-lg-6 col-lg-offset-3">
        <div class="row">
            <ul id="projects" class="list">
                <?php foreach($projects as $project){
                    echo '<li id="'.$project->id.'" class="project">',
                    '<a class="rm_project_cross" href="" >',
                    '<svg> <circle cx="12" cy="12" r="11" stroke="black" stroke-width="2" fill="white" />',
                    '<path stroke="black" stroke-width="4" fill="none" d="M6.25,6.25,17.75,17.75" />',
                    '<path stroke="black" stroke-width="4" fill="none" d="M6.25,17.75,17.75,6.25" />',
                    '</svg>',
                    '</a>',

                        '<a href="/index.php/Showroom/Project/'.$project->id.'" class="btn_project"><span>'.ucwords($project->name),
                    '<i class="fa fa-chevron-right"></i></span> </a> </li>';
                }
                ?>
            </ul>

            <div class="col-lg-12">
                <div id="add_input_project">
                    <?= form_open('Showroom', array('class' => 'form-inline'));
                    validation_errors()?>
                        <div class="form-group">
                            <label for="input_new_projet" > Nouveau Projet</label>
                            <input type="text" id="input_new_projet" name="name" class="form-control">
                        </div>
                        <button type="submit" id="link_add_project" class="btn btn-primary">Confirmer</button>
                    <?= form_close(); ?>
                </div>
            </div>
            </br>
            </br>
            </br>
            <div class="col-lg-12">
                <ul id="projects_edit">
                    <li class="project_edit" id="project_add">
                        <button class="btn btn-primary" id="add_project"> Nouveau projet </button>
                    </li>
                    <li class="project_edit" id="project_add">
                        <button class="btn btn-primary" id="rm_project"> Supprimer un projet </button>
                    </li>
                </ul>
            </div>
        </div>
    </div>
