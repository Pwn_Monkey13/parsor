<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function _getConfig()
{
    $CI =& get_instance();
    $CI->load->config('assets');
    $config = array();

    $config['path_base'] = $CI->config->item('path_base');
    $config['path_js']   = $CI->config->item('path_js');
    $config['path_css']  = $CI->config->item('path_css');
    $config['path_img']  = $CI->config->item('path_img');

    return $config;
}

function assets_css($files){

    $config = _getConfig();
    $path = base_url($config['path_base']);
    $css = null;
    if (is_array($files)) {
        foreach ($files as $file) {
            $css .= '<link rel="stylesheet" type="text/css" href="' . $path .'/'. $file . '">';
        }
    }
    return $css;
}

function assets_js($files){
    $config = _getConfig();
    $path = base_url($config['path_base']);
    $js = null;
    if (is_array($files)) {
        foreach ($files as $file) {
            $js .= '<script type="text/javascript" src="' . $path .'/'.$file . '"></script>';
        }
    }
    return $js;
}

function assets_img($file){
    return _assets_base($file, 'img');
}



