<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 07/04/2016
 * Time: 09:33
 */
class Ajax extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Config_Model');
        $this->load->model('Project_Model');
    }

    public function control_url()
    {
        if (isset($this->input)) {
            $url = $this->input->post('url');
            if ($this->bng->is_connected()) { //check if connection is set before trying to determine if $url is a link
                if (filter_var($url, FILTER_VALIDATE_URL)) {

                    //////////------------------------------////////////
                    $this->bng->Parse($url);
                    //////////------------------------------////////////

                    $data['struct'] = $this->bng->struct_gen;
                    $data['parsed'] = $this->bng->parsed_data;
                    $data['status'] = 'flux transmis (' . $this->bng->type . ')';
                    echo json_encode($data);
                }
            }
        } else echo false;
    }

    public function save_changes()
    {
        $idc = $this->input->post('idc');
        $data = $this->input->post('js_data');

        if (!is_null($data)) {
            json_decode($data, true);
            if ((json_last_error() == JSON_ERROR_NONE)) {
                $this->Config_Model->save_changes($idc, $data);
                echo true;
            } else echo false;

        } else echo null;
    }

    public function alter_config(){
        //$data = json_encode(array('name' => 'Config236'));
        //$data = json_decode($data, true);
        //$params = (object) array('idp' => '3', 'idc' => 5);

        $data = json_decode($this->input->post('data'), true);
        $params = json_decode($this->input->post('params'), true);

        if(is_array($data) && $params != null) {
            $resp = [];
            foreach ($data as $key => $single) {
                switch ($key) {
                    case 'name':
                        if (!$this->Config_Model->check_is_free_configname($params['idp'], $single)) {
                            $resp['msg'] = 'Nom de configuation déja utilisé';
                            $resp['id'] = 'error';
                        }
                        break;

                    case 'elements':
                        break;
                }

                if(empty($resp)){
                    var_dump($data);
                    $this->Config_Model->alter_config($params['idc'], $data);
                    $resp['msg'] = 'Modification bien prise en compte';
                    $resp['status'] = 'success';
                }
            }
            echo json_encode($resp);
        }
        else echo false;

        exit;
    }

    public function get_configs()
    {
        $id = $this->input->post('id');
        if ($id != null) {
            $configs = $this->Project_Model->get_configs($id);
            $status = true;
            echo json_encode(array('status' => $status, 'configs' => $configs));
        } else {
            $status = false;
            echo array('status' => $status);
        }
    }

    public function rm_project()
    {
        $idp = $this->input->post('idp');
        if ($idp != null) {
            $this->Project_Model->rm_project($idp);
            echo true;
        } else echo false;
    }

    public function add_project()
    {
        $name = $this->input->post('name');
        if ($name != null) {
            $this->Project_Model->add_project($name);
            echo true;
        } else echo false;
    }

    public function add_config()
    {
        $idprojet = $this->input->post('idprojet');
        $name = $this->input->post('name');

        if ($name != null & $idprojet != null) {
            $this->Config_Model->add_project($idprojet, $name);
            echo true;
        } else echo false;
    }

    public function rm_config()
    {
        $idc = $this->input->post('idc');

        if ($idc != null) {
            $this->Config_Model->rm_config($idc);
            echo true;
        } else {
            echo false;
        }
    }

    public function check_configname()
    {
        $name = $this->input->post('name');
        $idp = $this->input->post('idp');
        if ($name != null) {
            echo 'false';
        } elseif ($idp != null) {
            echo 'false';
        } elseif ($this->Config_Model->check_is_free_configname($idp, $name)) {
            echo 'true';
        } else {
            echo 'false';
        }
    }
}