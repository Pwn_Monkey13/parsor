<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 06/04/2016
 * Time: 15:08
 */
class Showroom extends BNG_Controller
{
    public function __construct(){
        parent::__construct();
    }

    public function index()
    {
        $this->data['error'] = null;
        $this->data['title'] = 'BNG Parsor - Showroom ';

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Nom du projet', 'required');

        if ($this->form_validation->run()) {
            $np = $this->input->post('name');

            if (!is_null($np)){
                if(!$this->Project_Model->check_is_free_project($np)){
                    $this->data['error'] = 'Le nom saisi est déja utilisé';
                }

                if($this->data['error'] == null){
                    $this->Project_Model->add_project($np);
                }
            }
        }

        $this->data['projects'] = $this->Project_Model->get_projects();
        $this->render('Showroom/projects_view');
    }

    public function Project($idp=null){
        if(is_null($idp))
            redirect('Showroom', 'refresh');

        $cp = $this->Project_Model->get_project_by_id($idp);
        if(is_null($cp)){
            redirect('Showroom', 'refresh');
        }

        $this->data['title'] = 'BNG Parsor - Project '.$cp->name;
        $this->data['cp'] = $cp;
        $this->data['configs'] =  $this->Project_Model->get_configs($cp->id);

        $this->render('Showroom/configs_view');
    }

    public function Config($idc){
        if(is_null($idc)){
            $this->data['error'] = 'Une erreur est intervenue';
            redirect('Showroom', 'refresh');
        }

        $cc = $this->Config_Model->get_config_by_id($idc);
        $cp = $this->Project_Model->get_project($cc->id_project);

        if(is_null($cc)){
            redirect('Showroom', 'refresh');
        }

        // 1 --
        $bng = $this->bngClass->parseFromBDD($cc->link,json_decode($cc->flux_match), $cc->recurrent_entry);

        // 2 --
        $xml = $this->bngClass->transform($bng);
        echo $xml;
        // 3 --
        $this->data['cc'] = $cc;
        $this->data['cp'] = $cp;
        $this->data['xml'] = $xml;
        $this->data['tree'] = $this->bngClass->getXMLTree($xml);
        $this->data['title'] = 'BngParsor - Config '.$cc->name;

        $this->render('Showroom/Config_view');
    }

    public function preview($idc, $item=null){
        if(is_null($idc)){
            $this->data['error'] = 'Une erreur est intervenue';
            redirect('Showroom', 'refresh');
        }

        $cc = $this->Config_Model->get_config_by_id($idc);

        if(is_null($cc)){
            redirect('Showroom', 'refresh');
        }

        if(isset($item)){
            $data = $this->bngClass->parseFromBDD($cc)[$item];
        }
        else{
            // 1 --
            $data = $this->bngClass->parseFromBDD($cc);
        }

        // 2 --
        $xml = $this->bngClass->transform($data);
        header('Content-type: text/xml');
        echo $xml;

        $this->load->view('Showroom/pre_view');
    }
}