<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 13/04/2016
 * Time: 13:25
 */

class Parsor extends BNG_Controller
{
    public $def_str;
    public $def_str_json;

    public function __construct()
    {
        parent::__construct();

        $this->def_str = array('id','rubrique','titre','image','langue','date','x','y','adresse',
            'tel','fax', 'mail', 'site','tarifs', 'descriptif');
    }

    public function index(){
        redirect('Showroom', 'refresh');
    }

    public function CreateConfig($idp=null)
    {
        $this->data['error'] = null;
        if (is_null($idp)) {
            $this->data['error'] = 'Une erreur est intervenue';
            redirect('Showroom', 'refresh');
        }

        $this->data['title'] = 'HomeParsor - NewConfig';

        //Current Project
        $cp = $this->Project_Model->get_project($idp);
        if($cp == null){
            redirect('Showroom', 'refresh');
        }

        $this->data['cp'] = $cp;
        $comma_str = implode(",", $this->def_str);
        $this->data['comma_str'] = $comma_str;


        $this->load->library('form_validation');
        $this->form_validation->set_rules('nameConf', 'Nom de la configuration', 'trim|required');
        $this->form_validation->set_rules('link', 'Lien du flux', 'trim|required');
        $this->form_validation->set_rules('elements', 'Elements de configuration', 'trim|required');

        if ($this->form_validation->run() == FALSE)
            $this->render('Parsor/CreateConfig_view');

        else {
            $nameConf = $this->input->post('nameConf');
            $link = $this->input->post('link');
            $elements = $this->input->post('elements');

            if (!$this->Config_Model->check_is_free_configname($cp->id, $nameConf))
                $this->data['error'] = 'Le nom saisi est déja utilisé';

            if($this->bngClass->is_valid_url($link)){
                $type = $this->bngClass->testType($link);

                if(!$type) {
                    $this->data['error'] = 'Le lien fourni n\'est pas parsable';
                }
            }
            else{
                $this->data['error'] = 'Le lien fourni n\'est pas valide';
            }

            $a = explode(",", $elements);
            if(!is_array($a)){
                $this->data['error'] = 'La liste des élements de destination n\'est pas valide';
            }


             //if evereything is allright
            if($this->data['error'] == null){
                $this->def_str = $a;
                $idc = $this->Config_Model->add_config($cp->id, $nameConf, $link, $elements, $type);

                redirect('Parsor/EditConfig/'.$idc);
            }
            else{
                $this->render('Parsor/CreateConfig_view');
            }
        }
    }

    public function EditConfig($idc=null)
    {
        if ($idc == null) {
            redirect('Showroom', 'refresh');
        }

        $cc = $this->Config_Model->get_config_by_id($idc);

        if ($cc == null) {
            redirect('Showroom', 'refresh');
        }
        $cp = $this->Project_Model->get_project($cc->id_project);

        $this->data['cc'] = $cc;
        $this->data['cp'] = $cp;
        $this->data['title'] = 'HomeParsor - NewConfig';
        $this->data['error'] = null;

        $elements = explode(",", $cc->elements);
        $this->data['elements'] = $elements;

        $this->load->library('form_validation');
        if($cc->type == 'json')
            $this->form_validation->set_rules('rec', 'Element récurrent', 'required');
        elseif($cc->type == 'xml'){
            $this->form_validation->set_rules('xmlns', 'Namespace global', 'required');
            $this->form_validation->set_rules('pathbase', 'Path de l\'élement récurrent', 'required');
        }

        $this->form_validation->set_rules('elements', 'Elements de configuration', 'trim|required');

        foreach ($elements as $item) {
            $this->form_validation->set_rules($item, ucfirst($item), 'trim');
        }

        // si le formulaire est chargé
        if ($this->form_validation->run()) {

            // $flux return Json Array or SimpleXML Object
            $flux = $this->bngClass->Parse($cc->link, $cc->type);

            if($cc->type == 'json') {
                //JSON
                $rec = $this->input->post('rec');
                $this->data['rec'] = $rec;


                if (gettype($rec) == 'string') {
                    $verif = ($this->bngClass->array_path_exists($flux, $rec)) ? $rec : false;
                }
            }

            elseif($cc->type == 'xml'){
                //XML
                $xmlns_input = $this->input->post('xmlns');
                $pathbase = $this->input->post('pathbase');

                $ns = $flux->getNamespaces(true);
                if(is_array($ns)){
                    foreach($ns as $key => $namespace){
                        //get default namespace witch key is empty
                        if($key == ''){
                            $flux->registerXPathNamespace($xmlns_input, $namespace);
                        }
                    }
                }
                $verif = (null !== $flux->xpath($pathbase)) ? $pathbase : false;
                }

            if (isset($verif) && $verif) {
                $this->data['success']['rec'] = 'Chemin d\'accés OK';
                $this->Config_Model->alter_config($cc->id, array('base_path' => $verif));
            }
            else {
                $this->data['error'] = 'Impossible de parser depuis "Element récurrent"';
                $inputs = null;
            }


            $elements = $this->input->post('elements');
            $a = explode(",", $elements);

            if (is_array($a)) {
                $this->Config_Model->alter_config($cc->id, array('elements' => $elements));
                $inputs = [];
                foreach ($a as $key => $item)
                {
                    $temp = $this->input->post($item);
                    if (gettype($temp) == 'string' && $temp != '') {
                        if ($this->input->post('control') === 'oui')
                        {
                            if($cc->type == 'json'){
                                $complete = $rec . ',0,' . $temp;

                                if ($this->bngClass->array_path_exists($flux, $complete)) {
                                    $inputs[$item] = $temp;
                                } else {
                                    $this->data['error'][$item] = 'Le champ ' . ucfirst($item) . ' ne match pas';
                                }
                            }

                            elseif($cc->type == 'xml'){
                                $complete = $pathbase.$temp;

                                if(!empty($flux->xpath($complete))){
                                    $inputs[$item] = $temp;
                                } else {
                                    $this->data['error'][$item] = 'Le champ ' . ucfirst($item) . ' ne match pas';
                                }
                            }
                        }
                        else {
                            $inputs[$item] = $temp;
                        }
                    }
                }
            }

            if (empty($this->data['error'])) {
                $json = json_encode($inputs);

                $this->Config_Model->alter_config($cc->id, array('flux_match' => $json));
                $this->data['success']['end'] = 'Modifications bien prise en compte';

              //  $this->Config_Model->save_changes($idc, $json);
                $cc = $this->Config_Model->get_config_by_id($idc);
                $this->data['cc'] = $cc;
            }
        }

        $this->render('Parsor/EditConfig_view');
    }
}