<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 04/04/2016
 * Time: 10:57
 */
class BngClass
{
    public $struct;  //structure générique made in BNG
    public $xml_format; // <?xml .....
    public $xml_container; // <item> </item>
    public $xml_final;

    public function __construct(){
        $this->struct = '';
        $this->xml_format = '<?xml version="1.0"?> <bng></bng>';
        $this->data_container = 'item';
    }




    // ***********   TOOLS   ********** //

    public function testType($url){
        if($this->isJson($url)){
            return 'json';
        }
        elseif($this->is_valid_xml($url)){
            return 'xml';
        }
        else
            return false;
    }

    public function is_valid_url($url){
        if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
            return true;
        } else {
            return false;
        }
    }

    public function is_valid_xml($file){
        $prev = libxml_use_internal_errors(true);
        $ret = true;

        try{
            new SimpleXMLElement($file, 0, true);
        }
        catch(Exception $e){
            $ret = false;
        }

        if(count(libxml_get_errors()) > 0){
            $ret = false;
        }

        libxml_clear_errors();
        libxml_use_internal_errors($prev);
        return $ret;
    }

    public function isJson($link){
        $string = file_get_contents($link);
        return ((is_string($string) &&
            (is_object(json_decode($string)) ||
                is_array(json_decode($string))))) ? true : false;
    }


    // ************  PARSOR TOOLS  ***********************//

    public function arrayPath(&$array, $path = array(), &$value = null)
    {
        $args = func_get_args();
        $ref = &$array;
        foreach ($path as $key) {
            if (!is_array($ref)) {
                $ref = array();
            }
            $ref = &$ref[$key];
        }
        $prev = $ref;
        if (array_key_exists(2, $args)) {
            // value param was passed -> we're setting
            $ref = $value;  // set the value
        }
        return $prev;
    }

    function array_path_exists(&$array, $path, $separator = ',')
    {
        $a =& $array;
        $paths = explode($separator, $path);
        $i = 0;
        foreach ($paths as $p) {
            if (isset($a[$p])) {
                if ($i == count($paths) - 1) {
                    return TRUE;
                }
                elseif(is_array($a[$p])) {
                    $a =& $a[$p];
                }
                else {
                    return FALSE;
                }
            }
            else {
                return FALSE;
            }
            $i++;
        }
    }

    //Parseur JSON
    function json_decode_array($link) {
        $json = (file_get_contents($link) != null) ? file_get_contents($link) : false;
        $from_json =  json_decode($json, true);
        return $from_json ? $from_json : $link;
    }

    public function parseXML($link){
        return (simplexml_load_file($link) != null) ? simplexml_load_file($link) : null;
    }


    // ***********   MAIN FUNCTION   ********** //

        public function Parse($link, $type){
            if ($type == 'json'){
                return $this->json_decode_array($link);
            }
            elseif ($type == 'xml'){
               // $xmlIterator = simplexml_load_file($link,'SimpleXMLIterator');
                return $this->parseXml($link);
            }
        }





    // ***********  TO XML  ********** //

    public function parseFromBDD($cc)
    {
        if ($cc->type == 'json')
            return $this->From_Json_to_array($cc);
        elseif ($cc->type == 'xml')
            return $this->From_XML_to_array($cc);
    }

    //load config files and parse Json flux in array
    public function From_Json_to_array($cc){
        $url = $cc->link;
        $obj_flux_match = json_decode($cc->flux_match);
        $basepath = $cc->base_path;

        $bng = [];
        $data = $this->json_decode_array($url);

        foreach ($data[$basepath] as $key => $item) {
            $single = [];
            foreach ($obj_flux_match as $element => $path) {
                //$pathv2 = $rec.','.$key.','.$path;
                $arraypath = explode(",", $path);
                //$arraypath = array_merge(array($rec,$key), $arraypath);

                $line = $this->arrayPath($item, $arraypath);
                $single[$element] = $line;
            }
            array_push($bng, $single);
        }
        return $bng;
    }


    //load config files and parse Json flux in array
    public function From_XML_to_array($cc){
        $url = $cc->link;
        $obj_flux_match = json_decode($cc->flux_match);
        $xmlns = $cc->namespace;
        $basepath = $cc->base_path;

        $bng = [];
        $data = $this->parseXML($url);

        $ns = $data->getNamespaces(true);
        if(is_array($ns)){
            foreach($ns as $key => $namespace){
                //get default namespace witch key is empty
                if($key == ''){
                    $data->registerXPathNamespace($xmlns, $namespace);
                }
            }
        }
        if(null !== $data->xpath($basepath)){
            foreach($obj_flux_match as $el_key => $path) {

                $input[$el_key] = $data->xpath($basepath.$path);
                $nbr_item = count($input[$el_key]);

                for($i=0;$i<$nbr_item;$i++){
                $bng[$i][$el_key] = $input[$el_key][$i];
                }
            }
        }
        return $bng;
    }



    public function transform($data){
        // creating object of SimpleXMLElement
        //$this->xml_final = new SimpleXMLElement($this->xml_format);

        // creating object of SimpleXMLElement
        $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');

        // function call to convert array to xml
        $this->arraytoxml($data,$xml_data);

        //saving generated xml file;
        return $xml_data->asXML();
    }

    function arraytoxml( $data, &$xml_data ) {
        foreach( $data as $key => $value ) {
            if( is_array($value) ) {
                if( is_numeric($key) ){
                    $key = 'item';
                }
                $subnode = $xml_data->addChild($key);
                $this->arraytoxml($value, $subnode);
            }
            else {
                $xml_data->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }

    public function getXMLTree($src){
        $xslt = new SimpleXMLElement('<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

               <xsl:template match="*">
                   <ul>
                        <li>
                        <label>
                                <xsl:value-of select="local-name()"/>
                                <xsl:text>  :  </xsl:text>
                                <xsl:apply-templates/>
                            </label>


                        </li>
                   </ul>
                </xsl:template>
            </xsl:stylesheet>');
        $xml = new SimpleXMLElement($src);
        $xsl_processor = new XSLTProcessor();
        $xsl_processor->importStylesheet($xslt);

        return $xsl_processor->transformToXml($xml);
    }

    //OLD

}