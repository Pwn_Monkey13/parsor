<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 21/04/2016
 * Time: 12:01
 */


//from array - One level Deep
 function looking_for_recurrent_el($data){
    $a = [];
    $i = 1;

    foreach ($data as $key => $item) {
        if (array_key_exists($key, $a)){
            $a[$key] = $i++;
        }
        else{
            $a[$key] = $i;
        }
    }
    $max = '';
    foreach($a as $key => $el){
        $max = $el;
        if($el > $max)
            $max = $el;
        $rec = $key;
    }
    return $rec;
}


//parseur recursif XML
function sxiToArray($sxi){
    $a = array();
    for( $sxi->rewind(); $sxi->valid(); $sxi->next() ) {
        if(!array_key_exists($sxi->key(), $a)){
            $a[$sxi->key()] = array();
        }

        if($sxi->hasChildren()){
            var_dump($sxi->current());
            $a[$sxi->key()][] = $this->sxiToArray($sxi->current());
        }

        else{
            $a[$sxi->key()][] = strval($sxi->current());
        }
    }
    return $a;
}

//parseur iteratif XML
 function parseXml($url)
{
    $a = [];
    $data = (simplexml_load_file($url) != null) ? simplexml_load_file($url) : null;

    foreach ($data as $key => $item) {

        if (array_key_exists($key, $a)){
            return $a[$key];
        }

        if (count($item->children()) != 0) {
            foreach ($item->children() as $subkey => $child) {
                if(count($child->children()) == 0 ){
                    if ($child->getNameSpaces(true) != null) {
                        $namespaces = $child->getNameSpaces(true);
                        $m = (isset($namespaces['m'])) ? true : false;
                        $d = (isset($namespaces['d'])) ? true : false;

                        if ($m && $d) {
                            $properties = (array)$child->children('m', TRUE)->properties->children('d', TRUE);
                            foreach($properties as $title => $property){
                                $a[$key][$subkey][$title] = $title.'  -- ex : '.$property;
                            }
                        }
                    }
                }
                else{
                    foreach ($child->children() as $subsubkey => $subchild) {
                        $a[$key][$subkey][$subsubkey] = $subsubkey.'  -- ex :  '.$subchild;
                    }
                }
            }
        }
    }
}


//load config files and parse XML flow in array
 function From_Xml_to_array($url, $obj_flux_match){
    $bng = [];
    $data = (simplexml_load_file($url) != null) ? simplexml_load_file($url) : null;
    $rec = $this->looking_for_recurrent_el($data);

    //var_dump($data);

    foreach ($data->$rec as $key => $item) {
        $single = [];

        foreach ($obj_flux_match as $bng_line => $flux_content) {
            $a = (array) $flux_content;
            $bng_title = current(array_flip($a));

            foreach ($item->children() as $childkey => $child) {
                if ($childkey == $bng_title) {
                    if (count($child->children()) == 0) { //entry->content
                        if ($child->getNameSpaces(true) != null) {
                            $namespaces = $child->getNameSpaces(true);
                            $m = (isset($namespaces['m'])) ? true : false;
                            $d = (isset($namespaces['d'])) ? true : false;

                            if ($m && $d) {
                                $properties = (array)$child->children('m', TRUE)->properties->children('d', TRUE);
                                foreach ($properties as $title => $property) {
                                    if ($title == $a[$bng_title]) {
                                        $single[$bng_line] = $property;
                                    }
                                }
                            }
                        }
                    } else { //others
                        foreach ($child->children() as $subchildkey => $subchild) {
                            if ($subchildkey == $a[$bng_title]) {
                                $single[$bng_line] = $subchildkey;
                            }
                        }
                    }
                }
            }
        }
        array_push($bng, $single);
    }
    return $bng;
}
