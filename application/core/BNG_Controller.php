<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 13/04/2016
 * Time: 13:16
 */
class BNG_Controller extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->data['css'] = array('css/bootstrap.min.css',
            'css/style.css',
            'css/DataTables-1.10.11/datatable.custom.css',
            'css/font-awesome.css');

        $this->data['js'] = array('js/jquery-2.1.4.js',
            'js/bootstrap.min.js',
            'js/datatable.custom.js',
            'js/bng.js');

        $this->load->library('bngClass', '', 'bngClass');
        $this->load->model('Config_Model');
        $this->load->model('Project_Model');
    }

    protected function render($view){
        $this->load->view('commun/header',$this->data);

        if(is_array($view)){
            foreach($view as $part){
                $this->load->view($part, $this->data);
            }
        }
        else {
            $this->load->view($view, $this->data);
        }

        $this->load->view('commun/footer',$this->data);

    }
}