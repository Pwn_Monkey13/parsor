data_parsed = [];
current_project = null;
all_projects_configs = [];
current_config = null;

//unused
$('#CreateCoenf').click(function(){
    var name = $('input#nameConf').val();
    var link = $('input#link').val();
    var elements = $('input#elements').val();

    //check if data is valid before send
    if(name.length == 0){
        alert('nom non saisi');
        return false;
    }

    else if(link.length == 0 ) {
        alert('lien non saisi');
        return false;
    }
    else if(elements.length == 0){
        alert('json non saisi');
        return false;
    }

    else if(!is_valid_url(link)){
        alert('pas un lien');
        return false;
    }

    else if(!IsJsonString(elements)){
        alert('pas un flux json');
        return false;
    }

    else{
        //control if name is already used
        var idp = $('input#hidden_idp').val();
        $.ajax({
            type: "POST",
            url: '/index.php/Ajax/check_configname',
            data: {idp : idp, name: name},
            dataType: "json",
            success: function(data){
                console.log(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(xhr.responseText);
                alert(thrownError);
            }
        });
    }
});



//unused
function is_valid_url(str) {
    var pattern = new RegExp('^((news|(ht|f)tp(s?)):\\/\\/)'+ // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
        '(\\#[-a-z\\d_]*)?$','i'); // fragment locater
    if(!pattern.test(str)) {
        return false;
    } else {
        return true;
    }
}

//unused
function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

//unused
function make_tbody(struct, parsed){
    $.each( struct, function( key, value ) {
        $('table#bng > tbody').append('<tr><th>'+key+' </th><th class="struct">'+value+'</th><th class="flux"><select></select></th></tr>');
        $.each( parsed, function( key, value ) {
            if(typeof(value) == 'object'){
                $('table#bng > tbody > tr > th:last > select').append('<optgroup id="further-option" label="'+key+'"></optgroup>');
                $.each( value, function( subkey ) {
                    $('table#bng > tbody > tr > th:last > select > optgroup').append('<option>'+subkey+'</option>');
                });
            }
        });
    });
}

//unsed
function make_blank_tbody_data(str,len) {
    for (var i = 0; i < len; i++) {
        $('#many_tables').append('<table id="'+ i+ '" class="bng_data" cellspacing="0" width="100%">' +
            '<thead><tr><th>Element du flux</th> <th>Contenu</th></tr> </thead> <tbody></tbody> </table>');
        var table = $('#many_tables > table#'+i);
        $(str).each(function (str_key, str_val) {
            table.children('tbody').append('<tr> <th class="element">'+str_val+'</th> <th class="contenu"><textarea> </textarea> </th></tr>');
        });
    }
}

//unused
function tbody_data(str) {
    $('#many_tables > table > tbody').each(function(table_key) {
        console.log(table_key);
        $(this).find('tr').each(function (struct_key) {
            var arr = $.makeArray(str[struct_key]);
            var container = arr[1];
            var content = arr[2];

            console.log(data_parsed);
            console.log(container);
            console.log(content);


            $(this).find('th.contenu > textarea').html(data_parsed[0].content[content]);
        });
    });
}