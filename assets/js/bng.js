var t = new Date();
str = {};

$(document).ready(function() {
   // alert(new Date().getTime() - t.getTime());
    $('#add_project').click(function(){
        $('#add_input_project').toggle();
    });

    $('#rm_project').click(function(){
        $('.rm_project_cross').toggle();
    });

    $('#rm_config').click(function(){
        $('.rm_config_cross').toggle();
    });

    $('body').on('click', 'a.rm_project_cross', function(event) {
        event.preventDefault();
        var idp = $(this).parent('li').attr('id');
        if(confirm("Comfirmer la suppression? --- Attention cette action supprimera le projet ainsi que l'ensemble de ses configurations !!")){
            $.ajax({
                type: "POST",
                url: '/index.php/Ajax/rm_project',
                data: {idp: idp},
                dataType: "json",
                success: function(){
                    location.reload();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(xhr.responseText);
                    alert(thrownError);
                }
            });
        }
        else{
            location.reload();
        }

    });

    $('body').on('click', 'a.rm_config_cross', function(event) {
        event.preventDefault();
        if(confirm("Comfirmer la suppression? --- Attention cette action supprimera le projet ainsi que l'ensemble de ses configurations !!")){
            var idc = $(this).parent('li').attr('id');
            console.log(idc);
            $.ajax({
                type: "POST",
                url: '/index.php/Ajax/rm_config',
                data: {idc: idc},
                dataType: "json",
                success: function(data){
                    console.log(data);
                    location.reload();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(xhr.responseText);
                    alert(thrownError);
                }
            });
        }
        else{
            location.reload();
        }
    });

    $('button#save').click(function () {

        $('select#selectDATA').each(function(){
            var element = $(this).parent('th').siblings('th.struct').html();
            var label = $(this.options[this.selectedIndex]).closest('optgroup').prop('label');
            var content = $(this.options[this.selectedIndex]).val();

            var line = {};
            line[label] = content;
            str[element] = line;
        });

        var idc = $('#hiddenIdConf').val();
        var js = JSON.stringify(str);
        console.log(typeof(js));
        $.ajax({
            type: "POST",
            url: '/index.php/Ajax/save_changes',
            data: {idc: idc,js_data: JSON.stringify(str)},
            dataType: "json",
            success: function(data){
                console.log(data);
                location.reload();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(xhr.responseText);
                alert(thrownError);
            },
            complete:function(){
                window.location.reload();
            }
        });

    });

    $('table').each(function(){
        $(this).DataTable({"paging": false
        }
        );
    });

    $('#construct').toggle();

    $('#nameConfEdit').change(function(){
        var params = {idp : $('#hiddenIdp').val(), idc : $('#hiddenIdConf').val()};
        var data = {name : $(this).val()};
        alter_config_ajax(params, data);

    });

    $('#linkEdit').change(function(){
        var params = {idp : $('#hiddenIdp').val(), idc : $('#hiddenIdConf').val()};
        var data = {elements : $(this).val()};
        alter_config_ajax(params, data);

    });

    if($('#hiddenState').length){
        var state = $('#hiddenState').val();
        if(state.length > 0){
            var state_array = $.parseJSON(state);
            state_array.each(function(key, content){
                console.log(content);
            });
        }
    }
});

function alter_config_ajax(p,d){
    var data = JSON.stringify(d);
    console.log(data);
    var params = JSON.stringify(p);

    $.ajax({
        url :'/index.php/Ajax/alter_config',
        type: "POST",
        data: {params: params, data: data},
        dataType: "json",
        success: function(data) {
            $('#msg_js').children('p').removeAttr( "class").attr('class', data.status).html(data.msg);
            console.log(data);
        },
        error: function(jqXHR, textStatus, errorThrow) {
            console.log(jqXHR, textStatus, errorThrow);
        },
        complete:function(){
            window.location.reload();
        }
    });
}


function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
